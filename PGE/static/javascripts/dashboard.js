$(function () {
    init(); //Intialize variables
    initPaint(); //Intialize sidebar Employees + Projects
    listen(); //Event listeners
    $("#project-emp").fastselect();
    $("#taskemp").fastselect();

});

function init() {
    user = {id: managerId};
    //team ID from server
    console.log(teamId);
    console.log("The manager id is");
    console.log(managerId)

    host = "http://139.59.24.25:4567/phantom/"+teamId+"/";
    endpoints = {
        //retreive projects
        projectsOfManager: host + "list/projects/manager/",
        projectsOfEmployee: host + "list/projects/employee/", //Used only in employee console

        //retreive employees
        employeeOfProjects: host + "list/employees/",
        employeeOfCompany: host + "list/employees",

        //Adding task,Project
        addTask: host + "web/assign/task",
        addProject: host + "web/create/project",

        //retreive project details
        projectDetails: host + "web/view/manager/",

        //userd only in employee console
        getTodo: host + "list/tasks/todo/",
        getDone: host + "list/tasks/done/",
        getDoing: host + "list/tasks/doing/",


        getDisjuncts: function (empid, taskid, tense) {
            return host + "list/" + empid + "/task/" + taskid + "/disjuncts/" + tense
        }
    };
    state = {projectId: 1};
}

function initPaint() {
    //Paint the Project List
    get.employeeOfCompany();
    get.projectsOfManager();

    //Paint the Emplyee list & Empoyee option in add project


}


function listen() {
    //Chat
    /*
        $(".send").click(function () {
            return addToChat($("textarea").val(), "left");
        });
    */
    if ( $('[type="date"]').prop('type') != 'date' ) {
        $('[type="date"]').datepicker();
    }

    //Project Tab Highlighter + action
    $(document).on('click', '#channel-block li', function () {
        //Highlighting
        $("#channel-block li").removeClass("active");
        $(this).addClass("active");

        //action
        var projectId = $(this).data("project-id");
        if (projectId) {
            get.employeeOfProject(projectId);
            get.projectDetail(projectId);
            state.projectId = projectId;
            $("#employee-view,#overview").addClass("hide");
            $("#tense").removeClass("hide");
        }
        else {
            paint.sidebar.employeeList(state.employeeOfCompany);
            state.projectId = 0;
            $("#project-view,#overview").removeClass("hide");
            $("#tense").addClass("hide");

        }
    });
    $(document).on('click', '#employee-block li', function () {
        $("#employee-block li").removeClass("active");
        $(this).addClass("active");
        $("#project-view").addClass("hide");
        $("#employee-view").removeClass("hide");

        var empid = $(this).data("emp-id");
        var tasks = state.projectDetails.filter(function(task){
            return task.employees.map(function(employee){return employee.id}).indexOf(empid) != -1
        });
        console.log("Employee tasks",tasks);
        get.employeePerformance(tasks,empid,state.projectId);


    });

    // used in form pseudo checkbox
    $(".checker").click(function () {
        var name = $(this).children("input").attr('name');
        console.log(name);
        $("[name =" + name + " ]").prop("checked", "false").parents(".checker").removeClass("selected");
        $(this).addClass("selected");
        $(this).children("input[type=radio]").prop("checked", "checked");
    });


    //Search Icon --> close icon toggling/Reset
    $("#search").keyup(function () {
        if ($(this).val() === "") {
            $("#search-icon").removeClass("fa-close").addClass("fa-search");
        }
        else {
            $("#search-icon").removeClass("fa-search").addClass("fa-close").click(function () {
                $("#employee-block ul li").slideDown(100);
                $("#search-icon").removeClass("fa-close").addClass("fa-search");
                $("#search").val("");
            });
        }
        filterEmployee($(this).val().toUpperCase());
    });
    $(".addTaskDialog").click(function () {
        $("#task-assignment").removeClass("hide");
    });
    $(document).on('click', ".card div div", function () {
        console.log("clicked");
        var taskId = $(this).data("id");
        var index = state.projectDetails.map(function(x) {return x.id; }).indexOf(taskId);
        var project = state.projectDetails[index];
        console.log(project, taskId,index);
        if(project.disjunct_set.length){
            var progress = Math.round(project.disjunct_set.filter(function(x){return x.stage.id === 3}).length/project.disjunct_set.length*100)

        }
        else{
            var progress = 100;
        }
        paint.modal.taskEnquiry(project,progress);

    });


}

//Filter Employee List //OK
function filterEmployee(filter) {
    var searchList = $("#employee-block ul li");
    searchList.each(function (index, employee) {
        if ($(this).text().toUpperCase().indexOf(filter) > -1) {
            $(this).slideDown(100);
        } else {
            $(this).slideUp(100);
        }
    });
}


function addToChat(message, position) {
    var chatMesage = "<div class='chatMessage " + position + "'>" + message + "</div>";
    $(".chatFlow").append(chatMesage);

}

//Drag & Drop
function allowDrop(event) {
    event.preventDefault();
}

function drag(event) {
    // event.preventDefault();
    event.dataTransfer.setData("text", event.target.innerHTML);
}

function drop(event) {
    // event.preventDefault();
    var name = event.dataTransfer.getData("text");
    $(event.target).append("<div>" + name + "</div>");
    console.log(name);
}

//listeners
//DOMgenerators
//contentproviders


// function getProgressForTask(task, tense) {
//
// }
//


var paint = {

    sidebar: {
        projectList: function (projects) {
            var DOM = "<li data-project-id='0' >Overview</li>";
            projects.forEach(function (project) {
                DOM += "<li data-project-id=" + project.id + ">" + project.project_name + "</li>";
            });
            $("#channel-block ul").html(DOM);
        },
        employeeList: function (employees) {
            var DOM = "";
            employees.forEach(function (employee) {
                DOM += "<li draggable='true' data-emp-id=" + employee.id + " onDragStart='drag(event)'>" + employee.user.first_name + "</li>";
            });
            $("#employee-block ul").html(DOM);
        }
    },
    fastSelect: {
        addProject: function (employees) {
            var DOM = "";
            employees.forEach(function (employee) {
                DOM += "<option value='" + employee.id + "'>" + employee.user.first_name + "</option>";
            });
            $("#project-emp").html(DOM);
        },
        addTask: function (employees) {
            var DOM = "";
            employees.forEach(function (employee) {
                DOM += "<option value='" + employee.id + "'>" + employee.user.first_name + "</option>";
            });
            $("#taskemp").html(DOM);
        }
    },
    tense: function (todo,doing,done) {
        [todo, doing, done].forEach(function (tense, index) {
            var DOM = "";
            tense.forEach(function (task) {
                DOM += "<div class ='uppercase'  data-tense='" + index + "' data-id='" + task.id + "'>" + task.task_name + "</div>";
            });
            switch (index) {
                case 0:
                    $(".card.low div").html(DOM);
                case 1:
                    $(".card.mid div").html(DOM);
                case 2:
                    $(".card.high div").html(DOM);
            }
        });
    },
    modal: {
        taskEnquiry: function (task,progress) {
            var DOM = "<div class='close fa fa-close'></div>";
            DOM += "<h2>" + task.task_name + "</h2><hr><ul class='inline no-style padding-8-0'>";
            task.employees.forEach(function (employee) {
                DOM += "<li class='padding-8-8 bg-col2 white rounded margin-8-8'>" + employee.user.first_name + "</li>"
            });
            DOM += "<h3>Deadline</h3><div>" + task.deadline + "</div>";
            DOM += "<progress-bar class='margin-16-0'><bar style='width:" + progress + "%'>" + progress + "%" + "</bar></progress-bar>";
            $("#task-enquiry div").html(DOM);
            $("#task-enquiry").removeClass("hide");
        },
    },
    performanceCard: function(tasks,empName) {
        var DOM = "";
        tasks.forEach(function(task){
            DOM += "<div class='card span-3 padding-8-8 margin-16-16'>";
            DOM += "<h3>"+task.task_name+"</h3>";
            if(!task.count){
                DOM += "<progress-bar><bar  style='width:100;background:white;color:black;'>Not Yet started</bar></progress-bar>";
            }
            else{
                DOM += "<progress-bar><bar style='width:"+task.progress+"%'>"+task.progress+"%</bar></progress-bar>";
            }
            DOM += "</div>";
        });
        $("#employee-view .title").text(empName);
        $("#employee-performance").html(DOM);

    }
}


var get = {
    projectsOfManager : function(){
        $.get(endpoints.projectsOfManager + user.id).done(function (data) {
            console.log("CompanyEmployees:", data);
            state.projectsOfManager = data;
            paint.sidebar.projectList(data);
        });
    },
    employeeOfCompany : function(){
        $.get(endpoints.employeeOfCompany).done(function (data) {
            console.log("Overview Employee:", data);
            state.employeeOfCompany = data;
            paint.sidebar.employeeList(data);
            paint.fastSelect.addProject(data);

        });
    },
    employeeOfProject : function(projectId){
        $.get(endpoints.employeeOfProjects+projectId).done(function (data) {
            console.log("Project Employee:", data);
            state.employeeOfProject = data;
            paint.sidebar.employeeList(data);
            paint.fastSelect.addTask(data);
           // $("#taskemp").fastSelect();
        });
    },
    projectDetail : function(projectId){
        $.get(endpoints.projectDetails+projectId).done(function(data){
            state.projectDetails = data;
            var todo = data.filter(function(x){return x.stage.id === 1});
            var doing = data.filter(function(x){return x.stage.id === 2});
            var done = data.filter(function(x){return x.stage.id === 3});
            paint.tense(todo,doing,done);
        });
    },
    employeePerformance : function(tasks,empId,projectId) {
        tasks.forEach(function (task,index) {
            $.when($.get(endpoints.getDisjuncts(empId, task.id, "done")), $.get(endpoints.getDisjuncts(empId, task.id, "count")))
                .done(function (done, count) {
                    tasks[index].done = done[0].length;
                    if(tasks[index].count = count[0].disjunct_count){
                        tasks[index].progress = Math.round(tasks[index].done / tasks[index].count * 100);
                    }
                    else{
                        tasks[index].progress = -1;
                    }


                });
        });
        console.log("Tasks",tasks);
        empName = state.employeeOfCompany.filter(function(employee){
            return employee.id == empId;
        });
        paint.performanceCard(tasks,empName[0].user.first_name);
    }


};

var post = {
    addTask: function(){
        var obj = {
            project_id : state.projectId,
            task_name : $("#task-assignment [name=name]").val(),
            deadline : $("#task-assignment [name=deadline]").val(),
            employee_id_list : $("#task-assignment [name=employees]").val()
        };
        console.log("post.addTask",JSON.stringify(obj));
        $.post(endpoints.addTask, JSON.stringify(obj)).done(function (data,status,xhr) {
            $("#task-assignment").addClass("hide");
            if(xhr.status == 200){
                get.projectDetail(state.projectId);
            }
            console.log(data);
        });

    },
    addProject: function(){
        var obj = {
            project_name: $("#add-channel-1 [name=project-name]").val(),
            manager_id: user.id,
            deadline: $("#add-channel-1 [name=deadline]").val(),
            employee_id_list: $("#project-emp").val()
        };
        console.log("Add project object:", JSON.stringify(obj));

        $.post(endpoints.addProject, JSON.stringify(obj)).done(function (data,status,xhr) {
            console.log(xhr.status, "project added");
            if(xhr.status == 200){
                get.projectsOfManager();
            }

        });
    }
};