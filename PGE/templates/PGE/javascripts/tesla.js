//To close modal
$(document).on('click','.close',function(){
    $(this).parents(".modal").addClass("hide");
});


// To toggle
$(document).on('click','[data-toggle]',function(){

    $($(this).data("toggle")).toggleClass("hide");
});

//To close
$(document).on('click','[data-close]',function(){
    $($(this).data("close")).addClass("hide");
});

//To open
$(document).on('click','[data-open]',function(){

    $($(this).data("open")).removeClass("hide");
    // $("#add-channel-1").show();
    console.log($(this).data("open"),"opened");
});
