from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from datetime import datetime, timedelta
from django.utils import timezone

from datetime import datetime

import pytz

tz = pytz.timezone('Asia/Calcutta')


class Role(models.Model):
    
    WEB_FRONT_DEVELOPER = "WFD"
    BACKEND_DEVELOPER = "BD"
    ANDROID_DEVELOPER = "AD"
    iOS_DEVELOPER = "ID"
    DESIGNER = "DS"

    ROLE_CHOICES = (
        (WEB_FRONT_DEVELOPER, "WebFrontendDeveloper"),
        (BACKEND_DEVELOPER, "BackendDeveloper"),
        (ANDROID_DEVELOPER, "AndroidDeveloper"),
        (iOS_DEVELOPER, "iOSDeveloper"),
        (DESIGNER, "Designer"),
    )

    role_name = models.CharField(choices=ROLE_CHOICES, max_length=3, default=ANDROID_DEVELOPER, unique=True)
    
    def __str__(self):
        return self.role_name


class Priority(models.Model):
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    magnitude = models.IntegerField()


class Team(models.Model):
    team_id = models.CharField(max_length=100, unique=True, default=None)
    team_name = models.CharField(max_length=100)
    access_token = models.CharField(max_length=250)

    def __str__(self):
        return self.team_name


class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=None)
    priority = models.ManyToManyField(Priority)
    unique_id = models.CharField(max_length=20, default=None, null=True)
    slack_bot_channel_id = models.CharField(max_length=20, default=None, null=True)
    team = models.ManyToManyField(Team)
    
    
    def __str__(self):
        return self.user.email  


class Manager(models.Model):
    employee_instance = models.OneToOneField(Employee)


class Selection(models.Model):
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    employees = models.ManyToManyField(Employee)


class Project(models.Model):
    project_name = models.CharField(max_length=100, default=None)
    start_date = models.DateField(default=datetime.now(tz=tz).date())
    end_date = models.DateField(default=None, null=True)
    manager = models.ForeignKey(Manager, default=None, null=True)
    employees = models.ManyToManyField(Employee)
    selections = models.ManyToManyField(Selection)  
    slack_channel_id = models.CharField(max_length=50, default=None, null=True)
    team = models.ForeignKey(Team, null=True, default=None)
    
    def __str__(self):
        return self.project_name


class Workingproject(models.Model):
    employee = models.OneToOneField(Employee)
    project = models.OneToOneField(Project)

    def __str__(self):
        return "Switched working project"


class Link(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    link_name = models.CharField(max_length=20)
    url = models.URLField(max_length=200)

    def __str__(self):
        return self.link_name


class Stage(models.Model):
    TODO = "TODO"
    DOING = "DOING"
    REVIEW = "REV"
    DONE = "DONE"

    STAGE_CHOICES = (
        (TODO, "TODO"),
        (DOING, "DOING"),
        (REVIEW, "REVIEW"),
        (DONE, "DONE"),
    )

    stage_name = models.CharField(max_length=10, choices=STAGE_CHOICES, default=TODO, unique=True)

    def __str__(self):
        return self.stage_name


class Task(models.Model):
    task_name = models.CharField(max_length=100, default=None)
    start_date = models.DateField(default=datetime.now(tz=tz).date())
    deadline = models.DateField(default=None, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    employees = models.ManyToManyField(Employee)
    stage = models.ForeignKey(Stage, default=None, null=True)

    def __str__(self):
        return self.task_name 


class Discussion(models.Model):
    task = models.ForeignKey(Task, default=None)
    users = models.ManyToManyField(Employee)
    from_date_time = models.DateTimeField(default=None, null=True)
    to_date_time = models.DateTimeField(default=None, null=True)

    def __str__(self):
        return "A discussion is scheduled"


class Disjunct(models.Model):
    disjunct_name = models.CharField(max_length=100, default=None)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, default=None)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    stage = models.ForeignKey(Stage)

    def __str__(self):
        return self.disjunct_name


class Pipe(models.Model):
    mentor = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name="mentor", default=None)
    mentee = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name="mentee", default=None)
    disjuncts = models.ManyToManyField(Disjunct)


class Preference(models.Model):
    employee = models.OneToOneField(Employee, on_delete=models.CASCADE)
    from_date_time = models.DateTimeField(default=None, null=True)
    to_date_time = models.DateTimeField(default=None, null=True)

    def __str__(self):
        return "A new discussion preference is set for the employee"

    
class Session(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    start_date_time = models.DateTimeField(default=datetime.now())
    end_date_time = models.DateTimeField(default=None, null=True)
    disjunct = models.ForeignKey(Disjunct, on_delete=models.CASCADE, default=None)
    is_master_session = models.BooleanField(default=False)
    
    def __str__(self):
        return self.user.email


class Absence(models.Model):

    WORK_FROM_HOME = "WFH"
    LEAVE = "LV"

    ABSENCE_TYPES = (
        (WORK_FROM_HOME, "Work from home"),
        (LEAVE, "Take a leave"),
    )


    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, default=None)
    start_date = models.DateField(default=None)
    end_date = models.DateField(default=None)
    absence_type = models.CharField(choices=ABSENCE_TYPES, max_length=3, default=LEAVE)
    is_approved = models.BooleanField(default=False)

    
    def __str__(self):
        return self.employee.user.email
