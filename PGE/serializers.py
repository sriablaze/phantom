from django.contrib.auth.models import User

from rest_framework import serializers

from PGE.models import Disjunct, Employee, Manager, Link, Priority, Project, Role, Stage, Task, Team

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'email',)


class PrioritySerializer(serializers.ModelSerializer):
    class Meta:
        model = Priority
        fields = ('magnitude',)


class EmployeeSerializer(serializers.ModelSerializer):
    
    priority = PrioritySerializer(read_only=True, many=True)
    user = UserSerializer(read_only=True)
    
    class Meta:
        model = Employee
        fields = ('id', 'user', 'priority', 'unique_id')


class ManagerSerializer(serializers.ModelSerializer):

    employee_instance = EmployeeSerializer(read_only=True)

    class Meta:
        model = Manager
        fields = ('id', 'employee_instance')


class StageSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Stage
        fields = ('id', 'stage_name')


class DisjunctSerializer(serializers.ModelSerializer):
    
    stage = StageSerializer(read_only=True)

    class Meta:
        model = Disjunct
        fields = ('id', 'disjunct_name', 'stage')


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'project_name',)


class LinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Link
        fields = ('link_name', 'url',)



class TaskSerializer(serializers.ModelSerializer):
    project = ProjectSerializer(read_only=True)
    employees = EmployeeSerializer(read_only=True, many=True)
    disjunct_set = DisjunctSerializer(read_only=True, many=True)
    stage = StageSerializer(read_only=True)

    class Meta:
        model = Task
        fields = ('id', 'task_name', 'deadline', 'project', 'employees', 'disjunct_set', 'stage')


class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ('access_token', 'team_id',)






